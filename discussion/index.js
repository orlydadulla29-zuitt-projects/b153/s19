// exponent operator

/*let firstNum = 8 ** 2
console.log(firstNum)
let secondNum = Math.pow(8, 2)
console.log(secondNum)*/


// Template literals

/*let greeting = "Hello! "

console.log(greeting + "My name is " + "Jino")

let song = "Careless Whisper"
let artist = "George Michael"

console.log("My favorite song is " + song + " by " + artist)

console.log(`My favorite song is ${song} by ${artist}`)*/

//Object Destructuring

const person = {
	firstName: "John",
	middleName: "Jacob",
	lastName: "Smith"
}

let = { firstName, middleName, lastName } = person

/*console.log(firstName)
console.log(middleName)
console.log(lastName)*/

const people = ["John", "Joe", "Jack"]

let [firstPerson, secondPerson, thirdPerson] = people

/*console.log(`The first person is ${firstPerson}`)
console.log(`The first person is ${secondPerson}`)
console.log(`The first person is ${thirdPerson}`)*/

//Arrow function

/*function addNum(num){
	return num+num
}*/

// const addNum = num => num + num


// Exclusive to Arrow functions, certain symbols can be removed if exactly ONE argument is needed for the function 

const addNum = num => num + num
