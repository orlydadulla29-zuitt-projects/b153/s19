const getCube = 3 ** 3;
console.log(`The cube of 3 is ${getCube}`)

const address = [ "258", "Washington Ave NW", "California", "90011"]
let [ houseNo, avenue, state, zipCode] = address
console.log(`I live at ${houseNo} ${avenue}, ${state} ${zipCode}`)

const animal = {
	name: "Lolong",
	species: "Saltwater Crocodile",
	weight: "1075 kgs",
	length: "20 ft",
	width: "3 in"
}

let = {name, species, weight, length, width} = animal
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${length} ${width}`)

numArr = [1,2,3,4,5]

numArr.forEach(number => console.log(number))